package br.edu.up.exemplo.endpoint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.up.exemplo.endpoint.service.EndpointService;

@CrossOrigin
@RestController
@RequestMapping("/endpoint")
public class EndpointController {
	
	@Autowired
	private EndpointService endpointService;
	
	@PostMapping("/{endpoint}")
	public void salvar(@PathVariable("endpoint") String endpoint, @RequestBody Object body) {
		endpointService.salvar(endpoint, body);
	}
	

}
