package br.edu.up.exemplo.endpoint.service;

import java.util.LinkedHashMap;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EndpointService {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Transactional
	public void salvar(String endpoint, Object body) {
		System.out.println(endpoint);
		System.out.println(body);

		String fields = "";
		String values = "";
		String comma = "";
		LinkedHashMap<String, Object> entityValues = (LinkedHashMap<String, Object>) body;

		for(String key : entityValues.keySet()) {
			fields += comma + key;
			values += comma + ":"+key;

			comma = ",";
		}

		String sql="INSERT into "+endpoint+"("+fields+")VALUES("+values+")";

		Query q = entityManager.createNativeQuery(sql);
		for(String key : entityValues.keySet()) {
			q.setParameter(key, entityValues.get(key));
		} 
		
		q.executeUpdate();		
	}

}
