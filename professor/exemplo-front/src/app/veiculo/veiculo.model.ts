export class VeiculoModel {

    guidVeiculo: number;
    placa: string;
    marca: string;
    modelo: string;

}