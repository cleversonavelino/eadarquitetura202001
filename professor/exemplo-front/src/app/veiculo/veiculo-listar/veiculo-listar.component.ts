import { Component, OnInit } from '@angular/core';
import { VeiculoModel } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';

@Component({
  selector: 'app-veiculo-listar',
  templateUrl: './veiculo-listar.component.html',
  styleUrls: ['./veiculo-listar.component.css']
})
export class VeiculoListarComponent implements OnInit {

  displayedColumns: string[] = ['id', 'placa', 'marca', 'modelo','alterar'];
  dataSource: any;

  /*veiculos: VeiculoModel[] = [
     {id: 1, placa: 'ABC-1234', modelo: 'XYZ', marca: 'XYZ'},
     {id: 2, placa: 'ABC-1234', modelo: 'XYZ', marca: 'XYZ'},
     {id: 3, placa: 'ABC-1234', modelo: 'XYZ', marca: 'XYZ'},
     {id: 3, placa: 'ABC-1234', modelo: 'XYZ', marca: 'XYZ'}
   ];*/

  constructor(private veiculoService: VeiculoService) { }

  ngOnInit() {
    this.veiculoService.listar().subscribe(veiculos => {
      this.dataSource = veiculos;
    },
      error => console.log(error),
      () => console.log('finalizou')
    );
  }

}
