import { Injectable } from '@angular/core';
import { VeiculoModel } from './veiculo.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {

  constructor(private http: HttpClient) { }

  salvar(veiculo: VeiculoModel): Observable<VeiculoModel> {
    return this.http.post<VeiculoModel>
      ('http://localhost:8080/api/veiculo', veiculo);
  }

  listar(): Observable<VeiculoModel[]> {
    return this.http.get<VeiculoModel[]>('http://localhost:8080/api/veiculo');
  }

  buscar(guidVeiculo: any): Observable<VeiculoModel> {
    return this.http.get<VeiculoModel>
      ('http://localhost:8080/api/veiculo/buscar/'+guidVeiculo);
  }

}
