import { Component, OnInit } from '@angular/core';
import { VeiculoModule } from '../veiculo.module';
import { VeiculoModel } from '../veiculo.model';
import { VeiculoService } from '../veiculo.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-veiculo-cadastro',
  templateUrl: './veiculo-cadastro.component.html',
  styleUrls: ['./veiculo-cadastro.component.css']
})
export class VeiculoCadastroComponent implements OnInit {

  veiculo: VeiculoModel = new VeiculoModel();

  constructor(private veiculoService: VeiculoService,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(paramMap => {
       let id = paramMap.get('id')
       if (id != null) {
         this.veiculoService.buscar(id).subscribe(veiculo => {
           this.veiculo = veiculo
         })
       }
    })

  }

  salvar() {
    this.veiculoService.salvar(this.veiculo).subscribe(veiculo => {
      console.log(veiculo);
    },
      error => console.log(error),
      () => console.log('finalizou'));
  }

}
