import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VeiculoListarComponent } from './veiculo/veiculo-listar/veiculo-listar.component';
import { VeiculoCadastroComponent } from './veiculo/veiculo-cadastro/veiculo-cadastro.component';
import { TemplateComponent } from './template/template/template.component';

const routes: Routes = [

  { path: 'veiculo-listar', component: VeiculoListarComponent },
  { path: 'veiculo-cadastro', component: VeiculoCadastroComponent },

  { path: '', redirectTo: 'view/veiculo-listar', pathMatch: 'full'  },

  {
    path: 'view',
    component: TemplateComponent, // this is the component with the <router-outlet> in the template
    children: [
      {
        path: 'veiculo-listar', // child route path
        component: VeiculoListarComponent, // child route component that the router renders
      },
      {
        path: 'veiculo-cadastro',
        component: VeiculoCadastroComponent, // another child route component that the router renders
      },
      {
        path: 'veiculo-cadastro/:id',
        component: VeiculoCadastroComponent, // another child route component that the router renders
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
